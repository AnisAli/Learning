import {Book,Librarian,DamageLogger,Author } from './interfaces';

class UniversityLibrarian implements Librarian {

    assistCustomer(cust:string):void{

        console.log(this.name+  " Librarian assisting "+ cust);
    }
    
    private _department: string;
    public get department(): string {
        return this._department;
    };
    public set department(value: string) {
        this._department = value;
    };

    private _email: string;
    public get email(): string {
        return this._email;
    };
    public set email(value: string) {
        this._email = value;
    };

    private _name: string;
    public get name(): string {
        return this._name;
    };
    public set name(value: string) {
        this._name = value;
    };
}

abstract class ReferenceItem {

    private _publisher:string;
    get Publisher():string{
        return this._publisher==undefined?"":this._publisher;
    }
    set Publisher(newPub:string){
        this._publisher = newPub.toLowerCase();
    }

    public static department:string = 'Research';


    constructor(public title:string, public year:number){
        console.log("Reference item constructor");
    }

    public PrintItem(){
        console.log(`${this.title} was published in ${this.year} by ${this.Publisher} [department: ${ReferenceItem.department}]`);
    }

    abstract printCitation():void;

}


class Encyclopedia extends ReferenceItem{
    //edition:number;

  constructor(newTitle:string, newYear:number, public edition: string,private test:number){
        super(newTitle,newYear);
          console.log('constructor encyclopedia');
  }
  

  PrintItem():void{
      super.PrintItem();
      console.log("Edition:"+ this.edition);
  }
 
    printCitation():void{
    console.log("Citation:"+ this.title);
    }

}

export {UniversityLibrarian,ReferenceItem, Encyclopedia};