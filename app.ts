import { Category } from './enums';
import { Book, DamageLogger, Librarian, Author, Magzine } from './interfaces';
import { UniversityLibrarian, ReferenceItem, Encyclopedia } from './classes';

import Shelf from './shelf';

import MediaCD from './lib/testDefault'




function GetAllBooks(): Book[] {
    let books = [
        { id: 1, title: 'C++ Learn', author: 'Alain', available: false, category: Category.Programming },
        { id: 2, title: 'Java Developer', author: 'Mark Ouies', available: true, category: Category.Programming },
        { id: 3, title: 'MVC Web Patterns', author: 'Todor Pelvis', available: false, category: Category.Programming },
        { id: 4, title: 'Sing a Song', author: 'Major Pelvis', available: true, category: Category.Poetry }
    ];
    return books;
}




function LogFirstAvailable(books): void {

    let numberofBooks = books.length;
    let firstAvailablebook = '';

    for (let book of books) {

        if (book.available) {
            firstAvailablebook = book.title;
            break;
        }
    }

    console.log("Total Books in Library :" + numberofBooks);
    console.log("First Available Book: " + firstAvailablebook);

}


function GetBookByCategory(categoryFilter: Category = Category.Poetry): Array<string> {

    console.log("Get books in Category: " + Category[categoryFilter]);
    let allBooks = GetAllBooks();
    let filteredTitles: string[] = [];
    allBooks.forEach(book => book.category == categoryFilter ? filteredTitles.push(book.title) : NaN);
    return filteredTitles;
}


function LogBookTitles(...titles: string[]): void {

    for (let title of titles) {
        console.log(title);
    }
}

function GetBookByID(id?: number): Book {

    const allBooks = GetAllBooks();
    return allBooks.filter(book => book.id == id)[0];
}

function CreateCustomerID(name: string, id?: number): string {
    return name + id;
}


function CheckoutBooks(customer: string, ...bookIDs: number[]): string[] {
    console.log("Checkout for Customer" + customer);

    let checkoutBooksTitle: string[] = [];

    bookIDs.forEach(id => {
        let book = GetBookByID(id);
        (book.available) ? checkoutBooksTitle.push(book.title) : NaN;
    })

    return checkoutBooksTitle;
}

function GetTitle(category: Category): string[];
function GetTitle(available: boolean): string[];
function GetTitle(bookProperty: any): string[] {

    const allBooks: Book[] = GetAllBooks();
    const foundTitle: string[] = [];

    if (typeof bookProperty == 'number') {
        allBooks.forEach(book => {
            if (book.category == bookProperty)
                foundTitle.push(book.title);
        });
    }
    if (typeof bookProperty == 'boolean') {
        allBooks.forEach(book => {
            if (book.available == bookProperty)
                foundTitle.push(book.title);
        });
    }
    return foundTitle;
}

function print(stringArr: string[]): void {
    stringArr.forEach(c => console.log(c));
}

function PrintBook(book: Book): void {
    console.log(book.title + ' [by ' + book.author + ']');
}

function LogAndReturn<T>(thing: T): T {
    console.log(thing);
    return thing;
}

//********************************************************************

/*


let refBook = new Encyclopedia("Dummer C++", 2010,"In2001",12);

LogAndReturn<Encyclopedia>(refBook);

refBook.Publisher="Pak Inc."
refBook.PrintItem();


let favLibrarian:Librarian = new UniversityLibrarian();
favLibrarian.name ="Karim";
favLibrarian.assistCustomer("Jamal");

let ref:ReferenceItem = new Encyclopedia("Robin Hood", 1998,"@001-",32);
ref.Publisher = 'Great Idiana Inc.'
ref.PrintItem();


let programmingBooks: string[] = GetBookByCategory(Category.Programming);
programmingBooks.forEach((val, idx, arr) => console.log(++idx + "-" + val + "--->" + arr.length));
console.log(GetBookByID(2).author);

const AllBooks = GetAllBooks();
LogFirstAvailable(AllBooks);

console.log("---------");

let x: number;
x = 5;

let IdGenerater: (chars: string, num: number) => string;
IdGenerater = CreateCustomerID;

let myID = IdGenerater("Karm", 1);
console.log(myID);

LogBookTitles('programmingBooks', 'asdad', 'asdasd');

console.log("------------------");

CheckoutBooks("Terrence", 3, 4).forEach(c => console.log(c));

console.log("--------");

var logDamage:DamageLogger;
logDamage = (damange:string) => console.log("Damange reported:"+ damange);

let myBook:Book = {
                id:5,
                title: 'Pride and Prejudics',
                author:'Arif Kardiwal',
                category: Category.History,
                available:true,
                markDamaged:logDamage
             //   year:'1983',
            //    copies:3
};

myBook.markDamaged("Wet by Customer");
PrintBook(myBook);
logDamage("Water spill");

console.log("--------");

let favoriteAuthor:Author = {email:"abc@ax.com", name:"Jimmy Wok", numBooksPublished:12};
let librarian:Librarian = {email:"abc@ax.com", name:"Jimmy Wok", department:"Computer Science", assistCustomer:logDamage};


let Newspaper  = class extends ReferenceItem{
     printCitation(){
         console.log("hello World");
     }
}

let a:ReferenceItem = new Newspaper("----",1965);
a.printCitation();
a.PrintItem();


*/

let inventory: Array<Book> = [
    { id: 1, title: 'C++ Learn', author: 'Alain', available: false, category: Category.Programming, toString: () => { console.log("1"); } },
    { id: 2, title: 'Java Developer', author: 'Mark Ouies', available: true, category: Category.Programming, toString: () => { console.log("2"); } },
    { id: 3, title: 'MVC Web Patterns', author: 'Todor Pelvis', available: false, category: Category.Programming, toString: () => { console.log("3"); } },
    { id: 4, title: 'Sing a Song', author: 'Major Pelvis', available: true, category: Category.Poetry, toString: () => { console.log("4"); } }
];


let BookShelf: Shelf<Book> = new Shelf<Book>();

inventory.forEach(c => BookShelf.add(c));

let firstBook: Book = BookShelf.getFirst();

BookShelf.printTitles();
// //let stringShelf: Shelf<string> = new Shelf<string>();

// let stringArray: string[] = [
//     "abc", "def", "ghi"
// ];

// stringArray.forEach(c => stringShelf.add(c));

