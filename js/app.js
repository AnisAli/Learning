"use strict";
var enums_1 = require('./enums');
var shelf_1 = require('./shelf');
function GetAllBooks() {
    var books = [
        { id: 1, title: 'C++ Learn', author: 'Alain', available: false, category: enums_1.Category.Programming },
        { id: 2, title: 'Java Developer', author: 'Mark Ouies', available: true, category: enums_1.Category.Programming },
        { id: 3, title: 'MVC Web Patterns', author: 'Todor Pelvis', available: false, category: enums_1.Category.Programming },
        { id: 4, title: 'Sing a Song', author: 'Major Pelvis', available: true, category: enums_1.Category.Poetry }
    ];
    return books;
}
function LogFirstAvailable(books) {
    var numberofBooks = books.length;
    var firstAvailablebook = '';
    for (var _i = 0, books_1 = books; _i < books_1.length; _i++) {
        var book = books_1[_i];
        if (book.available) {
            firstAvailablebook = book.title;
            break;
        }
    }
    console.log("Total Books in Library :" + numberofBooks);
    console.log("First Available Book: " + firstAvailablebook);
}
function GetBookByCategory(categoryFilter) {
    if (categoryFilter === void 0) { categoryFilter = enums_1.Category.Poetry; }
    console.log("Get books in Category: " + enums_1.Category[categoryFilter]);
    var allBooks = GetAllBooks();
    var filteredTitles = [];
    allBooks.forEach(function (book) { return book.category == categoryFilter ? filteredTitles.push(book.title) : NaN; });
    return filteredTitles;
}
function LogBookTitles() {
    var titles = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        titles[_i - 0] = arguments[_i];
    }
    for (var _a = 0, titles_1 = titles; _a < titles_1.length; _a++) {
        var title = titles_1[_a];
        console.log(title);
    }
}
function GetBookByID(id) {
    var allBooks = GetAllBooks();
    return allBooks.filter(function (book) { return book.id == id; })[0];
}
function CreateCustomerID(name, id) {
    return name + id;
}
function CheckoutBooks(customer) {
    var bookIDs = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        bookIDs[_i - 1] = arguments[_i];
    }
    console.log("Checkout for Customer" + customer);
    var checkoutBooksTitle = [];
    bookIDs.forEach(function (id) {
        var book = GetBookByID(id);
        (book.available) ? checkoutBooksTitle.push(book.title) : NaN;
    });
    return checkoutBooksTitle;
}
function GetTitle(bookProperty) {
    var allBooks = GetAllBooks();
    var foundTitle = [];
    if (typeof bookProperty == 'number') {
        allBooks.forEach(function (book) {
            if (book.category == bookProperty)
                foundTitle.push(book.title);
        });
    }
    if (typeof bookProperty == 'boolean') {
        allBooks.forEach(function (book) {
            if (book.available == bookProperty)
                foundTitle.push(book.title);
        });
    }
    return foundTitle;
}
function print(stringArr) {
    stringArr.forEach(function (c) { return console.log(c); });
}
function PrintBook(book) {
    console.log(book.title + ' [by ' + book.author + ']');
}
function LogAndReturn(thing) {
    console.log(thing);
    return thing;
}
//********************************************************************
/*


let refBook = new Encyclopedia("Dummer C++", 2010,"In2001",12);

LogAndReturn<Encyclopedia>(refBook);

refBook.Publisher="Pak Inc."
refBook.PrintItem();


let favLibrarian:Librarian = new UniversityLibrarian();
favLibrarian.name ="Karim";
favLibrarian.assistCustomer("Jamal");

let ref:ReferenceItem = new Encyclopedia("Robin Hood", 1998,"@001-",32);
ref.Publisher = 'Great Idiana Inc.'
ref.PrintItem();


let programmingBooks: string[] = GetBookByCategory(Category.Programming);
programmingBooks.forEach((val, idx, arr) => console.log(++idx + "-" + val + "--->" + arr.length));
console.log(GetBookByID(2).author);

const AllBooks = GetAllBooks();
LogFirstAvailable(AllBooks);

console.log("---------");

let x: number;
x = 5;

let IdGenerater: (chars: string, num: number) => string;
IdGenerater = CreateCustomerID;

let myID = IdGenerater("Karm", 1);
console.log(myID);

LogBookTitles('programmingBooks', 'asdad', 'asdasd');

console.log("------------------");

CheckoutBooks("Terrence", 3, 4).forEach(c => console.log(c));

console.log("--------");

var logDamage:DamageLogger;
logDamage = (damange:string) => console.log("Damange reported:"+ damange);

let myBook:Book = {
                id:5,
                title: 'Pride and Prejudics',
                author:'Arif Kardiwal',
                category: Category.History,
                available:true,
                markDamaged:logDamage
             //   year:'1983',
            //    copies:3
};

myBook.markDamaged("Wet by Customer");
PrintBook(myBook);
logDamage("Water spill");

console.log("--------");

let favoriteAuthor:Author = {email:"abc@ax.com", name:"Jimmy Wok", numBooksPublished:12};
let librarian:Librarian = {email:"abc@ax.com", name:"Jimmy Wok", department:"Computer Science", assistCustomer:logDamage};


let Newspaper  = class extends ReferenceItem{
     printCitation(){
         console.log("hello World");
     }
}

let a:ReferenceItem = new Newspaper("----",1965);
a.printCitation();
a.PrintItem();


*/
var inventory = [
    { id: 1, title: 'C++ Learn', author: 'Alain', available: false, category: enums_1.Category.Programming, toString: function () { console.log("1"); } },
    { id: 2, title: 'Java Developer', author: 'Mark Ouies', available: true, category: enums_1.Category.Programming, toString: function () { console.log("2"); } },
    { id: 3, title: 'MVC Web Patterns', author: 'Todor Pelvis', available: false, category: enums_1.Category.Programming, toString: function () { console.log("3"); } },
    { id: 4, title: 'Sing a Song', author: 'Major Pelvis', available: true, category: enums_1.Category.Poetry, toString: function () { console.log("4"); } }
];
var BookShelf = new shelf_1.default();
inventory.forEach(function (c) { return BookShelf.add(c); });
var firstBook = BookShelf.getFirst();
BookShelf.printTitles();
// //let stringShelf: Shelf<string> = new Shelf<string>();
// let stringArray: string[] = [
//     "abc", "def", "ghi"
// ];
// stringArray.forEach(c => stringShelf.add(c));
//# sourceMappingURL=app.js.map