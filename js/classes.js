"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var UniversityLibrarian = (function () {
    function UniversityLibrarian() {
    }
    UniversityLibrarian.prototype.assistCustomer = function (cust) {
        console.log(this.name + " Librarian assisting " + cust);
    };
    Object.defineProperty(UniversityLibrarian.prototype, "department", {
        get: function () {
            return this._department;
        },
        set: function (value) {
            this._department = value;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(UniversityLibrarian.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (value) {
            this._email = value;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Object.defineProperty(UniversityLibrarian.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    return UniversityLibrarian;
}());
exports.UniversityLibrarian = UniversityLibrarian;
var ReferenceItem = (function () {
    function ReferenceItem(title, year) {
        this.title = title;
        this.year = year;
        console.log("Reference item constructor");
    }
    Object.defineProperty(ReferenceItem.prototype, "Publisher", {
        get: function () {
            return this._publisher == undefined ? "" : this._publisher;
        },
        set: function (newPub) {
            this._publisher = newPub.toLowerCase();
        },
        enumerable: true,
        configurable: true
    });
    ReferenceItem.prototype.PrintItem = function () {
        console.log(this.title + " was published in " + this.year + " by " + this.Publisher + " [department: " + ReferenceItem.department + "]");
    };
    ReferenceItem.department = 'Research';
    return ReferenceItem;
}());
exports.ReferenceItem = ReferenceItem;
var Encyclopedia = (function (_super) {
    __extends(Encyclopedia, _super);
    //edition:number;
    function Encyclopedia(newTitle, newYear, edition, test) {
        _super.call(this, newTitle, newYear);
        this.edition = edition;
        this.test = test;
        console.log('constructor encyclopedia');
    }
    Encyclopedia.prototype.PrintItem = function () {
        _super.prototype.PrintItem.call(this);
        console.log("Edition:" + this.edition);
    };
    Encyclopedia.prototype.printCitation = function () {
        console.log("Citation:" + this.title);
    };
    return Encyclopedia;
}(ReferenceItem));
exports.Encyclopedia = Encyclopedia;
//# sourceMappingURL=classes.js.map