"use strict";
var Category;
(function (Category) {
    Category[Category["Biography"] = 0] = "Biography";
    Category[Category["Poetry"] = 1] = "Poetry";
    Category[Category["History"] = 2] = "History";
    Category[Category["Programming"] = 3] = "Programming";
})(Category || (Category = {}));
exports.Category = Category;
//# sourceMappingURL=enums.js.map